#!/usr/bin/env python3
import os
import sys
import time
import pathlib

PROGNAME = 'folderize'

# ##################################################################
def get_directory():
  ''' Scan current directory and return file and folder names '''
  files = []
  folders = []

  for item in pathlib.Path('./').glob('./*'):
    if item.is_file():
      files.append(item.name)
    else:
      folders.append(item.name)

  files.sort()
  folders.sort()

  return (files, folders)


# ##################################################################
def check_folders(files, folders):
  ''' Look for files with matching folders already existing '''
  matching = [f for f in files if get_folder_name(f) in folders]

  if matching:
    print(f"[ERROR] {len(matching)} folders found matching files in current folder")
    print(f"[ERROR] These files already have folders with the same name:")
    for f in matching:
      print(f"[ERROR]  - {f}")
    print(f"[ERROR] Please fix these before running {PROGNAME}")
    sys.exit(1)


# ##################################################################
def check_files(files):
  ''' Look for files with no extension '''
  noext = [f for f in files if '.' not in f]

  if noext:
    files = [f for f in files if f not in noext]
    print(f"[WARNING] {len(noext)} files found with no extension")
    print(f"[WARNING] These files will be ignored:")
    for f in noext:
      print(f"[WARNING]  - {f}")

  return files


# ##################################################################
def get_folder_name(f):
  ''' Compute the folder name for a given file '''
  return os.path.splitext(f)[0]


# ##################################################################
def move_files(files):
  ''' Create folders and move files '''
  ok = 0
  errors = 0

  for f in files:
    folder = get_folder_name(f)

    try:
      os.mkdir(folder)
    except:
      print(f"[ERROR] Could not create folder: {folder}/")
      errors += 1
      continue

    try:
      os.rename(f, f"{folder}/{f}")
    except:
      print(f"[ERROR] Could not move file: {f}")
      errors += 1
      continue

    ok += 1

  return (ok, errors)


# ##################################################################
def main():
  ''' Main program code '''
  print(f"{PROGNAME}")

  print("Scanning directory...")
  (files, folders) = get_directory()

  check_folders(files, folders)
  files = check_files(files)

  if not files:
    print("No files found.")
    sys.exit(0)

  print(f"{len(files)} files found.")
  print()
  print("New folders to be created:")
  print("--------------------------")
  for f in files:
    print(f"{get_folder_name(f)}/")
  print()

  response = input("Create folders and move files? [y/N] ").strip().upper()

  if response != 'Y':
    return

  (ok, errors) = move_files(files)

  print(f"{ok} files moved")
  if errors:
    print(f"{errors} errors found")

# ##################################################################

main()
