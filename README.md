# Folderize
Create a new folder for each file in the current directory and move each file inside its new folder.

## Dependencies
* Make sure you have [python 3](https://www.python.org/downloads/) installed
